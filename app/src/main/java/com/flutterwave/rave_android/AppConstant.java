package com.flutterwave.rave_android;

/**
 * @author .: Oriaghan Uyi
 * @email ..: oriaghan3@gmail.com, uyi.oriaghan@cwg-plc.com
 * @created : 6/27/18 9:48 AM
 */
public interface AppConstant {

    //LIVE
    public static String FLUTTER_WAVE_PUB_KEY_LIVE = "FLWPUBK-5a40fd87f636b9ffabce5c6e3ec7e257-X"; //live
    public static String FLUTTER_WAVE_SECRET_KEY_LIVE = "FLWSECK-a7610d1f5c5055b2f8c38b59f7ff5a40-X"; //live

    //STAGING
    public static String FLUTTER_WAVE_PUB_KEY_STAGING = "FLWPUBK-b99ede230dc504c364c282c1a08f0e10-X"; //stagging
    public static String FLUTTER_WAVE_SECRET_KEY_STAGING = "FLWSECK-fdd594634321f1ae54f957c9229c0c40-X"; //stagging

}
