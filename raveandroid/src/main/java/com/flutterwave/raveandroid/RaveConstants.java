package com.flutterwave.raveandroid;

/**
 * Created by hamzafetuga on 14/07/2017.
 */

public class RaveConstants {
    public static int PERMISSIONS_REQUEST_READ_PHONE_STATE = 419;

    public static String PUBLIC_KEY = "FLWPUBK-b99ede230dc504c364c282c1a08f0e10-X"; //test
    public static String PRIVATE_KEY = "FLWSECK-fdd594634321f1ae54f957c9229c0c40-X"; //test

    public static String STAGING_URL = "https://ravesandbox.azurewebsites.net";
    public static String LIVE_URL = "https://raveapi.azurewebsites.net";
    public static String VBV = "VBVSECURECODE";
    public static String GTB_OTP = "GTB_OTP";
    public static String NOAUTH = "NOAUTH";
    public static String PIN = "PIN";
    public static String AVS_VBVSECURECODE = "AVS_VBVSECURECODE";
    public static String NOAUTH_INTERNATIONAL = "NOAUTH_INTERNATIONAL";
    public static String RAVEPAY = "ravepay";
    public static String RAVE_PARAMS = "raveparams";
    public static int RAVE_REQUEST_CODE = 4199;
    public static int MANUAL_CARD_CHARGE = 403;
    public static int TOKEN_CHARGE = 24;
    public static boolean IS_PAYMENT_LIVE = false;
}
